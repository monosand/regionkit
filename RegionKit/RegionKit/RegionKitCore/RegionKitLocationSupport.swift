//
//  RegionKitLocationSupport.swift
//  RegionKit
//
//  Created by Francesco Luchena on 02/05/15.
//  Copyright (c) 2015 Francesco Luchena. All rights reserved.
//

import UIKit
import CoreLocation

let RegionKitLocationSupportUpdateLocationNotification = "RegionKitLocationSupportUpdateLocationNotification"

class RegionKitLocationSupport: NSObject, CLLocationManagerDelegate {
    
    var locationManager: CLLocationManager?
    static let sharedInstance = RegionKitLocationSupport()
    var lastUserLocation : CLLocation!
    
    override init() {
        super.init()
        
        
    }
    
    func updateCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager!.delegate = self
        locationManager!.desiredAccuracy = kCLLocationAccuracyBest
        if(locationManager!.respondsToSelector("requestAlwaysAuthorization")) {
            locationManager!.requestAlwaysAuthorization()
        }
        locationManager!.startUpdatingLocation()
    }
    
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        
        manager.stopUpdatingLocation()
        lastUserLocation = manager.location
        NSNotificationCenter.defaultCenter().postNotificationName(RegionKitLocationSupportUpdateLocationNotification, object: nil, userInfo: ["location" : manager.location.coordinate.latitude])
        
    }
    
    
    
}
