//
//  RegionKitTestAnnotationMap.swift
//  RegionKit
//
//  Created by Francesco Luchena on 30/04/15.
//  Copyright (c) 2015 Francesco Luchena. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

enum RegionKitTestAnnotationMapType: Int {
    case beaconRegion = 0
    case circularRegion
}

class RegionKitTestAnnotationMap: MKPointAnnotation {
    var annotationType : RegionKitTestAnnotationMapType!
    var radius : CLLocationDistance = 0
    var insideMe : Bool = false
}
