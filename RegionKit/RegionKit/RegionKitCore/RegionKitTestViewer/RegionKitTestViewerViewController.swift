//
//  RegionKitTestViewerViewController.swift
//  RegionKit
//
//  Created by Francesco Luchena on 29/04/15.
//  Copyright (c) 2015 Francesco Luchena. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

@objc(RegionKitTestViewerViewController) public class RegionKitTestViewerViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager() // Add this statement
    let regionKitSupportManager = RegionKitSupportManager.sharedInstance
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        // 1
        locationManager.delegate = self
        // 2
        locationManager.requestAlwaysAuthorization()
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "updateAnnotations",
            name: RegionKitSupportManagerRegionsStatesIsChanged,
            object: nil)

        // Do any additional setup after loading the view.
        
    }
    
    func updateAnnotations()
    {
        //zoomToUserLocationInMapView(mapView)
        for annotation in mapView.annotations
        {
            if let annotation = annotation as? MKAnnotation
            {
                mapView.removeAnnotation(annotation)
            }
        }
        if let x = mapView.overlays
        {
            for overlay in mapView.overlays
            {
                if let overlay = overlay as? MKOverlay
                {
                    mapView.removeOverlay(overlay)
                }
            }
        }
        
        for region : RegionKitSupportRegion in regionKitSupportManager.allMonitoredRegion
        {
            var annotationMap = RegionKitTestAnnotationMap()
            annotationMap.coordinate.latitude = region.location!.coordinate.latitude
            annotationMap.coordinate.longitude = region.location!.coordinate.longitude
            annotationMap.insideMe = region.insideMe
            if let circularRegion = region.region as? CLCircularRegion
            {
                annotationMap.radius = circularRegion.radius
                //mapView.addAnnotation(annotationMap)
            }
            self.addRadiusOverlayForGeotification(region)
        }
        
    }
    
    public func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        mapView.showsUserLocation = (status == .AuthorizedAlways)
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    public func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        if !(annotation is RegionKitTestAnnotationMap) {
            return nil
        }
        
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView.canShowCallout = true
        }
        else {
            anView.annotation = annotation
        }
        
        //Set annotation-specific properties **AFTER**
        //the view is dequeued or created...
        
        let regionKitTestAnnotationMap = annotation as! RegionKitTestAnnotationMap
        
        anView.image = UIImage(named:"annotationMap")
        return anView
    }
    
    func addRadiusOverlayForGeotification(region: RegionKitSupportRegion) {
        var circle : MKCircle!
        if let clRegion = region.region as? CLCircularRegion
        {
            circle = MKCircle(centerCoordinate: clRegion.center, radius: clRegion.radius)
            if region.insideMe
            {
                circle.title = "red"
            }
            else if (region.inQueen)
            {
                circle.title = "green"
            } else
            {
                circle.title = "gray"
            }
            
            if region.region.identifier == REGIONKIT_QUEEN_REGION_NAME
            {
                circle.subtitle = "queen"
            } else
            {
                circle.subtitle = "no queen"
            }
        } else
        {
            circle = MKCircle(centerCoordinate: region.location!.coordinate, radius: 50.0)
            if region.insideMe
            {
                circle.title = "purplue"
            } else
            {
                circle.title = "blue"
                
            }
            circle.subtitle = "no queen"
        }
        
        self.mapView.addOverlay(circle)
    }
    
    public func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKCircle {
            var circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.lineWidth = 1.0
            
            var color : UIColor = UIColor.purpleColor()
            if overlay.title == "red"
            {
                color = UIColor.redColor()
            } else if overlay.title == "green"
            {
                color = UIColor.greenColor()
            } else  if overlay.title == "gray"
            {
                color = UIColor.grayColor()
            } else  if overlay.title == "blue"
            {
                color = UIColor.blueColor()
            } else
            {
                color = UIColor.purpleColor()
            }
            if (overlay.subtitle == "queen")
            {
                circleRenderer.fillColor = color.colorWithAlphaComponent(0.2)
                color = UIColor.redColor()
            } else
            {
                circleRenderer.fillColor = color.colorWithAlphaComponent(0.4)
                
            }
            circleRenderer.strokeColor = color
            return circleRenderer
        }
        return nil
    }

}
