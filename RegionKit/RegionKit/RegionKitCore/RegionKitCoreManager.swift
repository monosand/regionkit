//
//  RegionKitCoreManager.swift
//  MRegionKit
//
//  Created by Francesco Luchena on 15/04/15.
//  Copyright (c) 2015 Francesco Luchena. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

let RegionKitCoreManagerEnterInRegionNotification = "RegionKitCoreManagerEnterInRegionNotification"
let RegionKitCoreManagerExitFromRegionNotification = "RegionKitCoreManagerExitFromRegionNotification"

class RegionKitCoreManager : NSObject, CLLocationManagerDelegate {
    var locationManager: RegionKitLocationManager?
    static let sharedInstance = RegionKitCoreManager()
    
    func stopTheseMonitoredRegions(var monitoredRegions: [CLRegion])
    {
        for monitoredRegion in monitoredRegions
        {
            locationManager!.stopMonitoringForRegion(monitoredRegion);
        }
    }
    
    func stopAllMonitoredRegion()
    {
        for region in RegionKitLocationManager().monitoredRegions
        {
            if let clregion = region as? CLRegion
            {
                RegionKitLocationManager().stopMonitoringForRegion(clregion);
            }
        }
    }
    
    func startMonitoringThesesRegions(var monitoredRegions: [CLRegion])
    {
        
        if locationManager == nil
        {
            locationManager = RegionKitLocationManager();
        }
        
        locationManager!.delegate = self
        
        if(locationManager!.respondsToSelector("requestAlwaysAuthorization")) {
            locationManager!.requestAlwaysAuthorization()
        }
        
        for monitoredRegion in monitoredRegions
        {
            locationManager!.startMonitoringForRegion(monitoredRegion)
        }
    }
    
    func locationManager(manager: CLLocationManager!,
        didEnterRegion region: CLRegion!) {
            NSLog("didEnterRegion");
    }
    
    func locationManager(manager: CLLocationManager!,
        didExitRegion region: CLRegion!) {
            NSLog("didExitRegion");
    }
    
    func locationManager(manager: CLLocationManager!,
        didDetermineState state: CLRegionState,
        forRegion region: CLRegion!)
    {
        if (state == CLRegionState.Inside){
            NSNotificationCenter.defaultCenter().postNotificationName(RegionKitCoreManagerEnterInRegionNotification, object: nil, userInfo: ["region" : region])
        } else if (state == CLRegionState.Outside) {
            NSNotificationCenter.defaultCenter().postNotificationName(RegionKitCoreManagerExitFromRegionNotification, object: nil, userInfo: ["region" : region])
        }else {
            NSLog("didDetermineState: unknow");
        }
    }
    
}
