//
//  RegionKitSupportRegion.swift
//  RegionKit
//
//  Created by Francesco Luchena on 29/04/15.
//  Copyright (c) 2015 Francesco Luchena. All rights reserved.
//

import UIKit
import CoreLocation

class RegionKitSupportRegion: NSObject {
    
    var region: CLRegion!
    var inQueen : Bool = false
    var insideMe: Bool = false
    
    var location : CLLocation?
    var radius : CLLocationDistance?
    
    override func isEqual(object: AnyObject?) -> Bool {
        if let object = object as? RegionKitSupportRegion {
            return region.identifier == object.region.identifier
        } else {
            return false
        }
    }

    
}
