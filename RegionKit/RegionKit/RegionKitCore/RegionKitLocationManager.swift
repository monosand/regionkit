//
//  RegionKitLocationManager.swift
//  RegionKit
//
//  Created by Francesco Luchena on 29/04/15.
//  Copyright (c) 2015 Francesco Luchena. All rights reserved.
//

import UIKit
import CoreLocation

let RegionKitLocationManagerStopMonitoringForRegion = "RegionKitLocationManagerStopMonitoringForRegion"
let RegionKitLocationManagerStartMonitoringForRegion = "RegionKitLocationManagerStartMonitoringForRegion"


class RegionKitLocationManager: CLLocationManager {
    override func stopMonitoringForRegion(region: CLRegion!) {
        super.stopMonitoringForRegion(region)
        NSNotificationCenter.defaultCenter().postNotificationName(RegionKitLocationManagerStopMonitoringForRegion, object: nil, userInfo: ["region" : region])
    }
    
    override func startMonitoringForRegion(region: CLRegion!) {
        super.startMonitoringForRegion(region)
        NSNotificationCenter.defaultCenter().postNotificationName(RegionKitLocationManagerStartMonitoringForRegion, object: nil, userInfo: ["region" : region])
    }
    
    
}
