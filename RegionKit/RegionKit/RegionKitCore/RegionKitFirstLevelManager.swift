//
//  RegionKitFirstLevelManager.swift
//  RegionKit
//
//  Created by Francesco Luchena on 01/05/15.
//  Copyright (c) 2015 Francesco Luchena. All rights reserved.
//

import UIKit
import CoreLocation

let REGIONKITMAXREGIONINQUEEN = 19
let REGIONKIT_QUEEN_REGION_NAME = "Queen Region"

class RegionKitFirstLevelManager: NSObject {
    
    var locationManager: CLLocationManager?
    static let sharedInstance = RegionKitFirstLevelManager()
    
    var managedRegions : [RegionKitSupportRegion] = []
    var inQueenRegions : [RegionKitSupportRegion] = []
    var queenRegion : RegionKitSupportRegion = RegionKitSupportRegion()
    
    override init() {
        super.init()
        
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "didEnterRegion:",
            name: RegionKitCoreManagerEnterInRegionNotification,
            object: nil)
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "didExitRegion:",
            name: RegionKitCoreManagerExitFromRegionNotification,
            object: nil)
        
        //inizializza il support manager
        RegionKitSupportManager.sharedInstance
        RegionKitCoreManager.sharedInstance.stopAllMonitoredRegion()

    }
    
    func manageTheseRegions(managedRegions newManagedRegions: [RegionKitSupportRegion])
    {
        //todo
        //Definire come comportanrsi in caso di cambio super region
        
        self.managedRegions = newManagedRegions;
        RegionKitSupportManager.sharedInstance.allMonitoredRegion = newManagedRegions
        self.determineInQueenRegions()
        
    }
    
    func determineInQueenRegions()
    {
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "determineInQueenRegionsAfterUpdateLocation:",
            name: RegionKitLocationSupportUpdateLocationNotification,
            object: nil)
        RegionKitLocationSupport.sharedInstance.updateCurrentLocation()
    }
    
    func determineInQueenRegionsAfterUpdateLocation(notification: NSNotification)
    {
        var currentLocation = RegionKitLocationSupport.sharedInstance.lastUserLocation
        var orderedManagedRegions = sorted(self.managedRegions,
            { (r1: RegionKitSupportRegion, r2: RegionKitSupportRegion) -> Bool in
            return r1.location?.distanceFromLocation(currentLocation) < r2.location?.distanceFromLocation(currentLocation)
            }
        )
    
        var newInQueenRegions : [RegionKitSupportRegion] = []
        if (orderedManagedRegions.count>0)
        {
            for i in 0...(min(orderedManagedRegions.count, REGIONKITMAXREGIONINQUEEN)-1)
            {
                newInQueenRegions.append(orderedManagedRegions[i])
            }
        }
        
        self.updateInQueenRegion(newInQueenRegions)
    }
    
    func updateInQueenRegion(let newInQueenRegions : [RegionKitSupportRegion])
    {
        let newSet = Set(newInQueenRegions)
        let oldSet = Set(inQueenRegions)
        let insideSet = Set(RegionKitSupportManager.sharedInstance.getInsideRegion())
        let holdSet = oldSet.intersect(insideSet).subtract(newSet.intersect(insideSet))
        
        
        let toStopSet = oldSet.subtract(insideSet).subtract(newSet)
        
        let numberToAddAtInQueen = REGIONKITMAXREGIONINQUEEN - holdSet.count
        var toAddOrHoldFromNewQueenRegion : [RegionKitSupportRegion] = []
        if (newInQueenRegions.count>0)
        {
            for i in 0...(numberToAddAtInQueen-1)
            {
                toAddOrHoldFromNewQueenRegion.append(newInQueenRegions[i])
            }
        }
        let toStartSet = Set(toAddOrHoldFromNewQueenRegion).subtract(oldSet).subtract(insideSet)
        
        
        RegionKitCoreManager.sharedInstance.stopTheseMonitoredRegions(getCLRegionFromSupportRegions(Array(toStopSet)))
        RegionKitCoreManager.sharedInstance.startMonitoringThesesRegions(getCLRegionFromSupportRegions(Array(toStartSet)))
        
        inQueenRegions.removeAll(keepCapacity: false)
        inQueenRegions = Array(holdSet.union(Set(toAddOrHoldFromNewQueenRegion)))
        
        if let region = queenRegion.region
        {
            RegionKitCoreManager.sharedInstance.stopTheseMonitoredRegions([queenRegion.region])
        }
        queenRegion = getQueenRegionFrom(inQueenRegions)
        RegionKitSupportManager.sharedInstance.updateNewQueenRegion(queenRegion)
        RegionKitCoreManager.sharedInstance.startMonitoringThesesRegions([queenRegion.region])
        
        
    }
    
    func didEnterRegion(notification: NSNotification) {
        
    }
    
    func didExitRegion(notification: NSNotification) {
        var region = notification.userInfo!["region"] as! CLRegion
        if (region.identifier == REGIONKIT_QUEEN_REGION_NAME)
        {
            self.determineInQueenRegions()
            println("uscito dalla Queen... ricalcolo")
        }
    }
    
    
    
}
