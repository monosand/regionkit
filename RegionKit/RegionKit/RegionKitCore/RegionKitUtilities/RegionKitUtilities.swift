//
//  RegionKitUtilities.swift
//  RegionKit
//
//  Created by Francesco Luchena on 30/04/15.
//  Copyright (c) 2015 Francesco Luchena. All rights reserved.
//

import Foundation
import UIKit
import MapKit

// MARK: Helper Functions

func showSimpleAlertWithTitle(title: String!, #message: String, #viewController: UIViewController) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
    let action = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
    alert.addAction(action)
    viewController.presentViewController(alert, animated: true, completion: nil)
}

func zoomToUserLocationInMapView(mapView: MKMapView) {
    if let coordinate = mapView.userLocation.location?.coordinate {
        let region = MKCoordinateRegionMakeWithDistance(coordinate, 1000, 1000)
        mapView.setRegion(region, animated: true)
    }
}

func getCLRegionFromSupportRegions(let supportRegions : [RegionKitSupportRegion]) -> [CLRegion]
{
    var clRegions : [CLRegion] = []
    for supportRegion : RegionKitSupportRegion in supportRegions
    {
        clRegions.append(supportRegion.region)
    }
    return clRegions
}

func determineQueenRadius (let inQueenRegions : [RegionKitSupportRegion], let currentLocation : CLLocation) -> Double
{
    var totalDistance : Double = 0.0
    var maxDistance : Double = 0.0
    var index : Int = 0
    if (inQueenRegions.count>0)
    {
        for inQueenRegion : RegionKitSupportRegion in inQueenRegions
        {
            if let location = inQueenRegion.location
            {
                var distance = location.distanceFromLocation(currentLocation) as Double
                maxDistance = max(distance, maxDistance)
                totalDistance += distance
                index++
            }
        }
        //return maxDistance
        return (totalDistance/Double(index))/2
    } else
    {
        return totalDistance
    }
}

func getQueenRegionFrom(let inQueenRegions : [RegionKitSupportRegion]) -> RegionKitSupportRegion
{
    var queenRegion = RegionKitSupportRegion()
    let clRegion = CLCircularRegion(center: RegionKitLocationSupport.sharedInstance.lastUserLocation.coordinate, radius: CLLocationDistance(determineQueenRadius(inQueenRegions, RegionKitLocationSupport.sharedInstance.lastUserLocation)), identifier: REGIONKIT_QUEEN_REGION_NAME)
    queenRegion.region = clRegion
    queenRegion.insideMe = true
    queenRegion.location = RegionKitLocationSupport.sharedInstance.lastUserLocation
    return queenRegion
}
