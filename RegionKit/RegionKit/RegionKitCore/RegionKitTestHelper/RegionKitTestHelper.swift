//
//  RegionKitTestHelper.swift
//  RegionKit
//
//  Created by Francesco Luchena on 27/04/15.
//  Copyright (c) 2015 Francesco Luchena. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

public class RegionKitTestHelper : NSObject, CLLocationManagerDelegate {
    var locationManager: CLLocationManager?
    public static let sharedInstance = RegionKitTestHelper()
    
    var myCurrentCircularRegion : CLCircularRegion!
    var circularRegions : [RegionKitSupportRegion] = []
    var beaconRegions : [RegionKitSupportRegion] = []
    
    
    var currentCircularRegions : [RegionKitSupportRegion] = []
    var currentBeaconRegions : [RegionKitSupportRegion] = []
    
    var beaconsMonitoredNumber = 0
    var circularsMonitoredNumber = 0
    var circularRadiusForPoi = 0
    var curcularRadiusForCurrentRegion = 0
    
    public override init() {
        super.init()
        var myDict: NSDictionary?
        if let path = NSBundle.mainBundle().pathForResource("RegionKitTestHelper", ofType: "plist") {
            myDict = NSDictionary(contentsOfFile: path)
        }
        if let propertyList = myDict {
            self.beaconsMonitoredNumber = propertyList.objectForKey("beaconsMonitoredNumber") as! Int
            self.circularsMonitoredNumber = propertyList.objectForKey("circularsMonitoredNumber") as! Int
            self.circularRadiusForPoi = propertyList.objectForKey("circularRadiusForPoi") as! Int
            self.curcularRadiusForCurrentRegion = propertyList.objectForKey("curcularRadiusForCurrentRegion") as! Int
        }
    }
    
    public func readCircularRegionFromFile() {
        let fileNameCoordinates = "coordinates"
        let fileNameBeacons = "beacons"
        
        //Read Circular
        if let path = NSBundle.mainBundle().pathForResource(fileNameCoordinates, ofType:"txt") {
            
            //reading
            let fileText = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)
            
            var fullDataSplit = split(fileText!) {$0 == "\n"}
            
            for index in 0...fullDataSplit.count-1
            {
                var singleDataSplit = split(fullDataSplit[index]) {$0 == ","}
    
                var latitude:CLLocationDegrees = (singleDataSplit[1] as NSString).doubleValue;
                var longitude:CLLocationDegrees = (singleDataSplit[3] as NSString).doubleValue;
                var center:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
                var radius:CLLocationDistance = CLLocationDistance(self.circularRadiusForPoi)
                var identifier:String = "circular \(index)"
                
                var clCircularRegion:CLCircularRegion = CLCircularRegion(center: center, radius: radius, identifier: identifier)
                
                var circularRegion = RegionKitSupportRegion()
                circularRegion.region = clCircularRegion
                circularRegion.location = CLLocation(latitude: latitude, longitude: longitude)
                
                circularRegions.append(circularRegion)
            }
            
        }
        
        //Read Beacons
        if let path = NSBundle.mainBundle().pathForResource(fileNameBeacons, ofType:"txt") {
            
            //reading
            let fileText = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)
            
            var fullDataSplit = split(fileText!) {$0 == "\n"}
            
            for index in 0...fullDataSplit.count-1
            {
                var singleDataSplit = split(fullDataSplit[index]) {$0 == ","}
                
                var proximityUDID = singleDataSplit[0]
                let clBeaconRegion : CLBeaconRegion;
                if let major = (singleDataSplit[1] as String).toInt()
                {
                    if let minor = (singleDataSplit[2] as String).toInt()
                    {
                        clBeaconRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: proximityUDID), major: UInt16(major), minor: UInt16(minor), identifier: "Beacon \(index)")
                    }
                    else
                    {
                        clBeaconRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: proximityUDID), major: UInt16(major), identifier: "Beacon \(index)")
                    }
                } else
                {
                    clBeaconRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: proximityUDID), identifier: "Beacon \(index)")
                }
                clBeaconRegion.notifyOnEntry = true
                clBeaconRegion.notifyOnExit = true
                
                
                var latitude:CLLocationDegrees = (singleDataSplit[3] as NSString).doubleValue;
                var longitude:CLLocationDegrees = (singleDataSplit[4] as NSString).doubleValue;
                
                var beaconRegion = RegionKitSupportRegion()
                beaconRegion.region = clBeaconRegion
                beaconRegion.location = CLLocation(latitude: latitude, longitude: longitude)
                
                beaconRegions.append(beaconRegion)
                
            }
            
        }
    }
    
    public func fireRandomMonitoring()
    {
        self.startRandomMonitoring()
    }
    
    private func startRandomMonitoring()
    {
        currentCircularRegions = []
        currentBeaconRegions = []
        
        //currentCircularRegions.append(myCurrentCircularRegion)
        let circularShuffle = circularRegions.shuffle()
        let beaconsShuffle = beaconRegions.shuffle()
        
        if self.circularsMonitoredNumber>0
        {
            for index in 0...circularShuffle.count-1
            {
                currentCircularRegions.append(circularShuffle[index])
            }
        }
        
        if beaconsMonitoredNumber>0
        {
            for index in 0...beaconsShuffle.count-1
            {
                currentBeaconRegions.append(beaconsShuffle[index])
            }
        }
    
//        let regionKitCoreManager = RegionKitCoreManager.sharedInstance
//        regionKitCoreManager.restartMonitorWithThesesRegions(currentCircularRegions, monitoredBeaconRegions: currentBeaconRegions)
        let regionKitFirstLevelManager = RegionKitFirstLevelManager.sharedInstance
        regionKitFirstLevelManager.manageTheseRegions(managedRegions: currentBeaconRegions+currentCircularRegions)
        
    }
    
}




extension Int {
    func random() -> Int {
        return Int(arc4random_uniform(UInt32(abs(self))))
    }
    func indexRandom() -> [Int] {
        var newIndex = 0
        var shuffledIndex:[Int] = []
        while shuffledIndex.count < self {
            newIndex = Int(arc4random_uniform(UInt32(self)))
            if !(find(shuffledIndex,newIndex) > -1 ) {
                shuffledIndex.append(newIndex)
            }
        }
        return  shuffledIndex
    }
}
extension Array {
    func shuffle() -> [T] {
        var shuffledContent:[T] = []
        let shuffledIndex:[Int] = self.count.indexRandom()
        for i in 0...shuffledIndex.count-1 {
            shuffledContent.append(self[shuffledIndex[i]])
        }
        return shuffledContent
    }
    mutating func shuffled() {
        var shuffledContent:[T] = []
        let shuffledIndex:[Int] = self.count.indexRandom()
        for i in 0...shuffledIndex.count-1 {
            shuffledContent.append(self[shuffledIndex[i]])
        }
        self = shuffledContent
    }
    func chooseOne() -> T {
        return self[Int(arc4random_uniform(UInt32(self.count)))]
    }
    func choose(x:Int) -> [T] {
        var shuffledContent:[T] = []
        let shuffledIndex:[Int] = x.indexRandom()
        for i in 0...shuffledIndex.count-1 {
            shuffledContent.append(self[shuffledIndex[i]])
        }
        return shuffledContent }
}
