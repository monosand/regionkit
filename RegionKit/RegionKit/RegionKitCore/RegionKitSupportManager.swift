//
//  RegionKitSupportManager.swift
//  RegionKit
//
//  Created by Francesco Luchena on 29/04/15.
//  Copyright (c) 2015 Francesco Luchena. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation


let RegionKitSupportManagerRegionsStatesIsChanged = "RegionKitSupportManagerRegionsStatesIsChanged"

class RegionKitSupportManager : NSObject {
    static let sharedInstance = RegionKitSupportManager()

    var allMonitoredRegion : [RegionKitSupportRegion] = []

    override init() {
        super.init()
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "didStartMonitoringForRegion:",
            name: RegionKitLocationManagerStartMonitoringForRegion,
            object: nil)
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "didStopMonitoringForRegion:",
            name: RegionKitLocationManagerStopMonitoringForRegion,
            object: nil)
        
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "didEnterRegion:",
            name: RegionKitCoreManagerEnterInRegionNotification,
            object: nil)
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "didExitRegion:",
            name: RegionKitCoreManagerExitFromRegionNotification,
            object: nil)
    }
    
    func updateNewQueenRegion(newQueenRegion: RegionKitSupportRegion)
    {
        for index in 0...allMonitoredRegion.count-1
        {
            let region = allMonitoredRegion[index]
            if (region.region.identifier == REGIONKIT_QUEEN_REGION_NAME)
            {
                allMonitoredRegion[index] = newQueenRegion
                return
            }
            
        }
        allMonitoredRegion.append(newQueenRegion)
        
    }
    
    func didStartMonitoringForRegion(notification: NSNotification) {
        var region = notification.userInfo!["region"] as! CLRegion
        NSLog("Start monitoring for region: \(region.description)");
        if var regionKitRegion = self.getMonitoredRegion(region)
        {
            regionKitRegion.inQueen = true
            regionKitRegion.region = region
        }
        NSNotificationCenter.defaultCenter().postNotificationName(RegionKitSupportManagerRegionsStatesIsChanged, object: nil, userInfo: nil)
    }
    
    func didStopMonitoringForRegion(notification: NSNotification) {
        var region = notification.userInfo!["region"] as! CLRegion
        NSLog("Stop monitoring for region: \(region.description)");
        if var regionKitRegion = self.getMonitoredRegion(region)
        {
            regionKitRegion.inQueen = false
            regionKitRegion.insideMe = false
        }
        NSNotificationCenter.defaultCenter().postNotificationName(RegionKitSupportManagerRegionsStatesIsChanged, object: nil, userInfo: nil)
    }
    
    func didEnterRegion(notification: NSNotification) {
        var region = notification.userInfo!["region"] as! CLRegion
        NSLog("Enter in region: \(region.description)");
        if var regionKitRegion = self.getMonitoredRegion(region)
        {
            regionKitRegion.insideMe = true
        }
        NSNotificationCenter.defaultCenter().postNotificationName(RegionKitSupportManagerRegionsStatesIsChanged, object: nil, userInfo: nil)
    }
    func didExitRegion(notification: NSNotification) {
        var region = notification.userInfo!["region"] as! CLRegion
        NSLog("Exit from region: \(region.description)");
        if var regionKitRegion = self.getMonitoredRegion(region)
        {
            regionKitRegion.insideMe = false
        }
        NSNotificationCenter.defaultCenter().postNotificationName(RegionKitSupportManagerRegionsStatesIsChanged, object: nil, userInfo: nil)
    }
    
    func getMonitoredRegion(clRegion : CLRegion) -> RegionKitSupportRegion?
    {
        for region : RegionKitSupportRegion in allMonitoredRegion
        {
            if (region.region.identifier == clRegion.identifier)
            {
                return region
            }
        }
        return nil
    }
    
    func getInsideRegion() -> [RegionKitSupportRegion]
    {
        let filteredArray = self.allMonitoredRegion.filter(( { (region: RegionKitSupportRegion) -> Bool in
            return region.insideMe == true
        }))
        return filteredArray
    }
    
}