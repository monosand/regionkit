//
//  RegionKitFramework.h
//  RegionKitFramework
//
//  Created by Francesco Luchena on 15/05/15.
//  Copyright (c) 2015 Francesco Luchena. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for RegionKitFramework.
FOUNDATION_EXPORT double RegionKitFrameworkVersionNumber;

//! Project version string for RegionKitFramework.
FOUNDATION_EXPORT const unsigned char RegionKitFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RegionKitFramework/PublicHeader.h>


